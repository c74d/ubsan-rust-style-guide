
html: style-guide.html

view-html: html
	@chromium-browser style-guide.html

style-guide.html: style-guide.adoc
	@nix-shell -p asciidoctor \
		--run 'asciidoctor -b xhtml5 -a toc style-guide.adoc'

pdf: style-guide.pdf

view-pdf: pdf
	@chromium-browser style-guide.pdf

style-guide.pdf: style-guide.adoc
	@nix-shell -p asciidoctor \
		--run 'asciidoctor -r asciidoctor-pdf -b pdf style-guide.adoc'

clean:
	@rm style-guide.html
	@rm style-guide.pdf
